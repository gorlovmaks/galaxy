@extends('layouts.master')

<div  style="width: 700px; margin: 0 auto">
    <div class="row no-gutter">
        <div class="block-top-extreme no-padding">
            <div class="step_cells home1">
                <div class="box "></div>
            </div>
        </div>

            @include('home.blocks.block_top')

        <div class="block-top-extreme no-padding">
            <div class="step_cells home2">
                <div class="box"></div>
            </div>
        </div>

        <div style="clear: both"></div>
            @include('home.blocks.block_left')

        <div class="block-top  no-padding center_galaxy">
            @include('bones.dices')
        </div>

            @include('home.blocks.block_right')
        <div style="clear: both"></div>

        <div class="block-top-extreme  no-padding">
            <div class="step_cells home3">
                <div class="box"></div>
            </div>
        </div>

            @include('home.blocks.block_bottom')

        <div class="block-top-extreme no-padding">
            <div class="step_cells home4">
                <div class="box"></div>
            </div>
        </div>

    </div>
</div>




