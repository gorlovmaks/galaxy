<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Галактика</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="img/logo_ico.png" type="image/png">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="login/css/style.css">
    <base target="_parent">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/background_dice.css">
</head>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<base target="_parent">
<a href="{{route('logout')}}">ВЫЙТИ</a>
<body>


    @yield('content')
</body>
</html>

<script>
    window.addEventListener( 'DOMContentLoaded', function () {

        const buttonRoolDice = document.querySelector( '.dice-roll' );

        function rollDice () {

            const diceSide1 = document.getElementById( 'dice-side-1' );
            const diceSide2 = document.getElementById( 'dice-side-2' );
            const status = document.getElementById( 'status' );

            const side1 = Math.floor( Math.random() * 6 ) + 1;
            const side2 = Math.floor( Math.random() * 6 ) + 1;
            const diceTotal = side1 + side2;

            diceSide1.innerHTML = side1;
            diceSide2.innerHTML = side2;

            status.innerHTML = 'У вас ' + side1 + ' и ' +side2+'.';

        }

        buttonRoolDice.addEventListener( 'click', rollDice, false );

    }, false);
</script>