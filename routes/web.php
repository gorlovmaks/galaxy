<?php


/**
 * Подписка пользователя
 */
Route::group(['prefix' => 'galaxy'], function () {
    Route::get('/', 'HomeController@index')->name('galaxy.home');
});

Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout','Auth\LoginController@logout')->name('logout');

//Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');